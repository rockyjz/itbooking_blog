package com.itbooking.vo;

public class OrderVo {

    private String ordernumber;
    private String productid;
    private Integer status;
    private Long orderid;


    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    @Override
    public String toString() {
        return "OrderVo{" +
                "ordernumber='" + ordernumber + '\'' +
                ", productid='" + productid + '\'' +
                ", status=" + status +
                '}';
    }
}
