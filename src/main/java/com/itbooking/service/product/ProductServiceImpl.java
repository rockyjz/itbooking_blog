package com.itbooking.service.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbooking.mapper.ProductMapper;
import com.itbooking.pojo.ProductDo;
import com.itbooking.vo.ProductVo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProductServiceImpl implements  IProductService {


    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询所有得产品
     * @return
     */
    public IPage<ProductDo> findProducts(ProductVo productVo){
        //1：设置分页
        Page<ProductDo> page = new Page<>(productVo.getPageNo(),productVo.getPageSize());

        //2: 设置条件
        QueryWrapper<ProductDo> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq(productVo.getCid()!=null,"categoryid",productVo.getCid());

        // 把状态是发布得并且是未删除得查询返回
        queryWrapper.eq("status",1);
        queryWrapper.eq("isdelete",0);
        // 根据创建排序
        queryWrapper.orderByDesc("createtime");
        //queryWrapper.orderByDesc("hits");
        //queryWrapper.orderByDesc("loves");

        //3: 执行分页查询
        IPage<ProductDo> productDoIPage = productMapper.selectPage(page, queryWrapper);

        //4:返回
        return productDoIPage;

    }



    /**
     * 根据id查询产品明细
     * @param id
     * @return
     */
    public ProductDo getProductDo(Long id){
        return productMapper.selectById(id);
    }


    /**
     * 上下架商品
     * @param id
     * @param status
     * @return
     */
    public int upDownProducts(Long id,Integer status){
        ProductDo productDo = new ProductDo();
        productDo.setId(id);
        productDo.setStatus(status);
        return productMapper.updateById(productDo);
    }
}
