package com.itbooking.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itbooking.core.annotation.LoginToken;
import com.itbooking.pojo.ProductDo;
import com.itbooking.service.product.IProductService;
import com.itbooking.vo.ProductVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/product")
@Api(tags = "产品模块")
public class ProductController {

    @Autowired
    private IProductService productService;


    @GetMapping("page")
    @ApiIgnore // 不生成文档
    @Deprecated //标记放弃方法
    public IPage<ProductDo> findProducts(ProductVo productVo){
        return productService.findProducts(productVo);
    }

    // 布置作业：根据id查询产品明细，并且对接接口，一定去查看自己编写的代码，总结哦
    // 布置作业：百度搜索：微信小程序滚动分页 + 两个数组如何拼接 （去了解一下js 数组的api） concat unshift push spllice filter map reduce forEach
    // 了解一个mysql中 float double decimal得区别。解决方案：varchar来解决 + java代码转换计算
    // "23.234" === Double.parseDouble("23.234")

    /**
     * 根据id查询产品明细
     * @param id
     * @return
     */
    @GetMapping("get/{id}")
    @LoginToken
    // 2：描述方法
    @ApiOperation(value="根据产品id查询产品信息 - getProductDo")
    // 3: 参数描述
    // 3-1 :如果一个参数只加@ApiImplicitParam
    // 3-2 :如果多个参数只加@ApiImplicitParams(@@ApiImplicitParam(xxxx),@ApiImplicitParam(xxxx).....)
    // paramType 有三个值： path, query, body和 "" 不写也不有问题
    // xxx.com?id=1&name=zhangsa-----query
    // xxx.com---post请求放在请求体中-----body
    // xxx.com/detail/1/zhangsan------path
    @ApiImplicitParam(name = "id",value="产品ID",defaultValue = "1",dataType = "Long",dataTypeClass=Long.class,required = true,paramType = "path")
    public ProductDo getProductDo(@PathVariable("id") Long id){
        return productService.getProductDo(id);
    }

    @ApiImplicitParams({
        @ApiImplicitParam(name = "id",value="产品ID",defaultValue = "1",dataType = "Long",dataTypeClass=Long.class,required = true,paramType = "path"),
        @ApiImplicitParam(name = "name",value="产品的名字",paramType = "query")
    })
    @GetMapping("get2/{id}")
    @LoginToken
    @ApiOperation(value="根据产品id和产品名字查询产品信息 - getProductDo2")
    public ProductDo getProductDo2(@PathVariable("id") Long id,String name){
        return productService.getProductDo(id);
    }
}
