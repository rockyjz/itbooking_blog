package com.itbooking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itbooking.pojo.UserDo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<UserDo> {
}
