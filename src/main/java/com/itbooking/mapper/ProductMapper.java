package com.itbooking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itbooking.pojo.ProductDo;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductMapper extends BaseMapper<ProductDo> {
}
