package com.itbooking.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName("ib_user")
public class UserDo {
    // 主键
    @TableId
    private Long id;
    // 用户名
    private String nickName;
    //头像
    private String avatarUrl;
    //手机
    private String telephone;
    // 用户名
    private String openid;
    // 密码
    private String password;
    // 年龄
    private Integer age;
    // 创建时间
    private Date createtime;
}
