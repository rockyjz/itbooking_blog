package com.itbooking.service.product;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itbooking.pojo.ProductDo;
import com.itbooking.vo.ProductVo;

public interface IProductService {

    /**
     * 查询所有得产品
     * @return
     */
    IPage<ProductDo> findProducts(ProductVo productVo);


    /**
     * 根据id查询产品明细
     * @param id
     * @return
     */
    ProductDo getProductDo(Long id);


    public int upDownProducts(Long id,Integer status);

}
