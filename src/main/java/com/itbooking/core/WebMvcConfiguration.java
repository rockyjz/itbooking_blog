package com.itbooking.core;

import com.itbooking.core.interceptor.AuthIntercetor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration {

    @Bean
    public AuthIntercetor getAuthIntercetor() {
        return new AuthIntercetor();
    }


    // 如果你在异步请求的过程中出现cors-corss错误，就说明跨域了
    // 那么你就必须进行下面的配置
    @Bean
    public WebMvcConfigurer corConfiguration() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET", "POST", "DELETE", "PUT", "PATCH");
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(getAuthIntercetor()).addPathPatterns("/api/**");
            }
        };
    }

}
