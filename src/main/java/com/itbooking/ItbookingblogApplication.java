package com.itbooking;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.itbooking.mapper")
public class ItbookingblogApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItbookingblogApplication.class, args);
    }
}
