package com.itbooking.web;

import com.itbooking.core.annotation.LoginToken;
import com.itbooking.core.jwt.JwtOpendIdUtil;
import com.itbooking.service.WeixinLoginService;
import com.itbooking.vo.WeixinResultData;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
@Log4j2
@Api(tags = "登录token")
public class TokenController {

    @Autowired
    WeixinLoginService weixinLoginService;

    /**
     * token获取的方法，
     * 并且会注册一个微信用户到数据库表中
     * 1：可以通过他获取到openid和session_key
     * 2: 注册用户
     * 3：获取token
     * @param code
     * @return
     */
    @GetMapping("token")
    @LoginToken
    public WeixinResultData token(String code){
        return weixinLoginService.getWeixinOpendId(code);
    }

}
