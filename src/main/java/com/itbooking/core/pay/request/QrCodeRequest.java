package com.itbooking.core.pay.request;

import com.alibaba.fastjson.JSON;
import com.itbooking.core.pay.ReportReqData;
import com.itbooking.core.pay.ReportReqDataApp;
import com.itbooking.core.pay.config.WeiXinConfig;
import com.itbooking.core.pay.config.WeiXinConstants;
import com.itbooking.core.pay.sign.Signature;
import com.itbooking.core.pay.util.*;
import lombok.extern.log4j.Log4j2;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;


/**
 * 数据提交并且生成二维码
 * WeixinSubmit<BR>
 * 创建人:小威 <BR>
 * 时间：2015年10月16日-下午2:14:47 <BR>
 *
 * @version 1.0.0
 */
@Log4j2
public class QrCodeRequest {

    //private static Logger log = LoggerFactory.getLogger(QrCodeRequest.class);

    /**
     * 微信支付成功信息配置
     * 方法名：submitWeixinMessage<br/>
     * 创建人：mofeng <br/>
     * 时间：2018年11月28日-下午2:06:06 <br/>
     * 手机:1564545646464<br/>
     *
     * @param data
     * @return Map<String, String><br/>
     * @throws <br/>
     * @since 1.0.0<br />
     */
    public static Map<String, String> submitWeixinMessage(ReportReqData data, String key) {
        try {
            data.setTrade_type(data.getTrade_type());//扫一扫支付方式 NATIVE
            data.setSign(Signature.getSign(data.toMap(), key));//参数签名字符 md5
            log.info("调用接口");

            // 核心1：这里会把微信支付服务器需要的参数携带给微信然后进行校验，如果成功就返回xml数据格式
            String returnData = WeiXinRequest.submitData(WeiXinConstants.URL, data.toMap());
            System.out.println("======================>" + returnData);
            if (returnData != null && !"".equals(returnData)) {
                log.info("当前支付成功返回的数据: ============>{}",returnData);
                // 核心2 ：对返回的xml数据格式进行解析。
                Map<String, String> map = XMLParser.getMapFromXML(returnData);//解析返回的字符串 并且组成map集合
                System.out.println("==============================>" + map);
                if (null != map && !map.isEmpty()) {
                    String resultCode = (String) map.get(WeiXinConstants.RESULT);
                    // 核心3：校验成功，就会返回SUSCESS
                    if ("SUCCESS".equals(resultCode)) {//链接生成成功

                        String params = data.getAttach().replace("'", "\"");
                        Map<String, String> nmap = new HashMap<>();

                        //核心4：就把微信小程序支付窗口需要的参数，进行封装返回。
                        nmap.put("appId", data.getAppid());
                        nmap.put("timeStamp", System.currentTimeMillis() + "");
                        nmap.put("signType", "MD5");
                        nmap.put("nonceStr", data.getNonce_str());
                        nmap.put("package", "prepay_id=" + map.get("prepay_id"));
                        // 注意！注意！注意！ 上面的五个参数顺序可以随便调整，但是不能放在签名之后。
                        nmap.put("paySign", Signature.getSign(nmap, key));

                        // 这两个是额外参数，是我返回给小程序可能会使用到的参数
                        nmap.put("attach",params);
                        nmap.put("ordernumber",data.getOut_trade_no()+"");
                        return nmap;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 微信分账支付
     * 方法名：submitWeixinMessage<br/>
     * 创建人：mofeng <br/>
     * 时间：2018年11月28日-下午2:06:06 <br/>
     * 手机:1564545646464<br/>
     *
     * @param data
     * @return Map<String, String><br/>
     * @throws <br/>
     * @since 1.0.0<br />
     */
    public static Map<String, String> submitWeixinMchMessage(ReportReqDataMch data,String key) {
        try {
            //	data.setNonce_str(RandomStringGenerator.getRandomStringByLength(32));//随机字符
            log.info("开始调用分账接口");
            data.setSign(Signature.HMACSHA256(data.toMap(), key));//签名字符
            log.info("当前执行分账的参数: {},key为: {}", JSON.toJSONString(data), key);
            String returnData = WeiXinRequest.subMchData(WeiXinConstants.MCH_URL, data.toMap(), data.getMch_id());
            System.out.println("======================>" + returnData);
            log.info("当前执行分账返回的数据: ============>{}", returnData);
            if (returnData != null && !"".equals(returnData)) {
                Map<String, String> resultMap = XMLParser.getMapFromXML(returnData);//解析返回的字符串 并且组成map集合
                System.out.println("==============================>" + resultMap);
                if (null != resultMap && !resultMap.isEmpty()) {
                    return resultMap;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



    public static Map<String, Object> submitWeixinMessageAPP(ReportReqDataApp data, String key) {
        try {
            data.setSign(Signature.getSign(data.toMap(), key));//签名字符
            String returnData = WeiXinRequest.submitData(WeiXinConstants.URL, data);
            System.out.println("======================>" + returnData);
            if (returnData != null && !"".equals(returnData)) {
                Map<String, String> map = XMLParser.getMapFromXML(returnData);//解析返回的字符串 并且组成map集合
                System.out.println("==============================>" + map);
                if (null != map && !map.isEmpty()) {
                    String resultCode = (String) map.get(WeiXinConstants.RESULT);
                    if ("SUCCESS".equals(resultCode)) {//链接生成成功
                        Map<String, Object> nmap = new HashMap<>();
                        nmap.put("appid", data.getAppid());
                        nmap.put("partnerid", data.getMch_id());
                        nmap.put("timestamp", System.currentTimeMillis());
                        nmap.put("noncestr", map.get("nonce_str"));
                        nmap.put("package", "Sign=WXPay");
                        nmap.put("prepayid", map.get("prepay_id"));
                        nmap.put("sign", data.getSign());
                        return nmap;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 生成二维码
     * (这里用一句话描述这个方法的作用)<BR>
     * 方法名：submitCode<BR>
     * 创建人：小威 <BR>
     * 时间：2015年10月16日-下午5:23:27 <BR>
     *
     * @param data
     * @return BufferedImage<BR>
     * @throws <BR>
     * @since 1.0.0
     */
    public static BufferedImage submitCode(ReportReqData data) {
        data.setAppid(WeiXinConfig.APPID);//公众账号ID
        data.setMch_id(WeiXinConfig.MCH_ID);//商户号
        data.setNonce_str(RandomStringGenerator.getRandomStringByLength(32));//随机字符
        data.setTrade_type("NATIVE");//扫一扫支付方式
        data.setSign(Signature.getSign(data.toMap()));//签名字符
        BufferedImage buff = null;
        //请求提交
        try {
            String returnData = WeiXinRequest.submitData(WeiXinConstants.URL, data);
            if (returnData != null && !"".equals(returnData)) {
                Map<String, String> map = XMLParser.getMapFromXML(returnData);//解析返回的字符串 并且组成map集合
                if (null != map && !map.isEmpty()) {
                    String resultCode = (String) map.get(WeiXinConstants.RESULT);
                    if ("SUCCESS".equals(resultCode)) {//链接生成成功
                        buff = QrCode.QrcodeImage(WeiXinConstants.WIDTH, WeiXinConstants.HEIGHT, String.valueOf(map.get(WeiXinConstants.CODE_URL)));
                    }
                }
            }
            return buff;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 检查订单是否已经支付
     * (这里用一句话描述这个方法的作用)<BR>
     * 方法名：checkPay<BR>
     * 创建人：小威 <BR>
     * 时间：2015年10月17日-上午10:54:02 <BR>
     *
     * @param data
     * @return boolean<BR> true 已经支付   false没有支付
     * @throws <BR>
     * @since 1.0.0
     */
    public static boolean checkPay(ReportReqData data) {
        data.setAppid(WeiXinConfig.APPID);//公众账号ID
        data.setMch_id(WeiXinConfig.MCH_ID);//商户号
        data.setNonce_str(RandomStringGenerator.getRandomStringByLength(32));//随机字符
        data.setTrade_type("NATIVE");//扫一扫支付方式
        data.setSign(Signature.getSign(data.toMap()));//签名字符
        try {
            String returnData = WeiXinRequest.submitData(WeiXinConstants.URL, data);
            if (returnData != null && !"".equals(returnData)) {
                Map<String, String> map = XMLParser.getMapFromXML(returnData);//解析返回的字符串 并且组成map集合
                if (null != map && !map.isEmpty()) {
                    String resultCode = (String) map.get(WeiXinConstants.RESULT);
                    if ("FAIL".equals(resultCode) && "ORDERPAID".equals(map.get(WeiXinConstants.ERROR_CODE))) {//该订单已经支付
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
