package com.itbooking.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itbooking.mapper.UserMapper;
import com.itbooking.pojo.UserDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserServiceImpl implements IUserService {


    @Autowired
    private UserMapper userMapper;

    public UserDo getUser(Long id) {
        return userMapper.selectById(id);
    }

    public UserDo saveUserDo(UserDo userDo) {
        int count = userMapper.insert(userDo);
        return count > 0 ? userDo : null;
    }

    /**
     * 获取openid获取用信息
     *
     * @param openid
     * @return
     */
    public UserDo getUserByOpenid(String openid) {
        QueryWrapper<UserDo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid", openid);
        UserDo userDo = userMapper.selectOne(queryWrapper);
        return userDo;
    }

}
