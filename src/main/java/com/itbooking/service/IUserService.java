package com.itbooking.service;

import com.itbooking.pojo.UserDo;

public interface IUserService {

    UserDo getUser(Long id);
    UserDo saveUserDo(UserDo userDo);
    UserDo getUserByOpenid(String openid);
}
