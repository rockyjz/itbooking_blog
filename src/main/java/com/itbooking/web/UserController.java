package com.itbooking.web;

import com.itbooking.pojo.UserDo;
import com.itbooking.service.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
@Api(tags = "用户管理")
public class UserController {


    @Autowired
    private IUserService userService;

    @GetMapping("/getuser/{id}")
    public UserDo getUser(@PathVariable("id")Long id){
        return userService.getUser(id);
    }
}
