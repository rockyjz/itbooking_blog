package com.itbooking.vo;

import com.itbooking.pojo.UserDo;

public class WeixinResultData {
    private String token;
    private UserDo userDo;
    private WeixinData weixinData;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserDo getUserDo() {
        return userDo;
    }

    public void setUserDo(UserDo userDo) {
        this.userDo = userDo;
    }

    public WeixinData getWeixinData() {
        return weixinData;
    }

    public void setWeixinData(WeixinData weixinData) {
        this.weixinData = weixinData;
    }
}
