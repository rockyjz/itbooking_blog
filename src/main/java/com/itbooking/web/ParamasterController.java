package com.itbooking.web;

import com.itbooking.pojo.UserDo;
import com.itbooking.service.IUserService;
import com.itbooking.vo.UserVo;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
    @RequestMapping("/api/model")
@Log4j2
@Api(tags = "参数测试")
public class ParamasterController {

    @Autowired
    private IUserService userService;

    @GetMapping("/get1/{id}")
    public UserDo getUser1(@PathVariable("id")Long id){
    //public UserDo getUser1(@PathVariable Long id){
        log.info("1---getUser--->你的参数是：id:{}",id);
        return userService.getUser(id);
    }

    // /get2?id=1
    @GetMapping("/get2")
    public UserDo getUser2(Long id){
        log.info("1---getUser2--->你的参数是：id:{}",id);
        return userService.getUser(id);
    }

    @GetMapping("/get3")
    public UserDo getUser3(Long id,String name){
        log.info("1---getUser3--->你的参数是：id:{}，name:{}",id,name);
        //log.info("1---getUser3--->你的参数是：id:"+id+"，name:"+name);
        return userService.getUser(id);
    }

    @GetMapping("/get4")
    public UserVo getUser4(UserVo userVo){
        log.info("1---getUser4--->你的参数是：uservo：{}",userVo);
        return userVo;
    }


    // post请求
    @PostMapping("/save")
    public UserDo save(Long id){
        log.info("2---save--->你的参数是：id:{}",id);
        return userService.getUser(id);
    }


    @PostMapping("/save2")
    // application/json + {id:1234,name:1345} uservo + @requestbody
    public UserDo save2(Long id,Long name){
        log.info("2---save2--->你的参数是：id:{},name:{}",id,name);
        return userService.getUser(id);
    }

    @PostMapping("/save3")
    public UserVo save3(UserVo userVo){
        log.info("2---save3--->你的参数是：userVo:{}",userVo);
        return userVo;
    }

    // 未来如果你的参数是[] list map
    // 一般的处理方案，你一定要使用springmvc的数据类型转换
    @PostMapping("/save4")
    public Map<String,Object> save4(Map<String,Object> params){
        log.info("2---save4--->你的参数是：params:{}",params);
        return params;
    }
}
