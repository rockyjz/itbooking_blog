package com.itbooking.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel
public class ProductVo {

    private Integer pageNo=1;
    private Integer pageSize=10;
    private Long cid;
    private String keyword;

}
