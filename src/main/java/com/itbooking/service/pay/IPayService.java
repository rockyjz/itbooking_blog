package com.itbooking.service.pay;

import com.itbooking.pojo.OrderDo;
import com.itbooking.vo.OrderVo;

/**
 * 订单处理的Service层
 */
public interface IPayService {

    public OrderDo mainPayOperator(OrderVo orderVo);
}
