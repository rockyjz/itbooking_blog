package com.itbooking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itbooking.pojo.OrderDo;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends BaseMapper<OrderDo> {
}
