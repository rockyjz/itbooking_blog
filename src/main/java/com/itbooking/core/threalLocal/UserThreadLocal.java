package com.itbooking.core.threalLocal;

import com.itbooking.pojo.UserDo;

/**
 * 用户线程得副本类
 */
public class UserThreadLocal {

    private static ThreadLocal<UserDo> userDoThreadLocal = new ThreadLocal<>();

    public static void put(UserDo userDo){
        userDoThreadLocal.set(userDo);
    }

    public static UserDo get(){
        return userDoThreadLocal.get();
    }

    public static void remove(){
        userDoThreadLocal.remove();
    }
}
